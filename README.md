# TERRA API by SK Consult

## Dependencies

### helmet

_Help us to secure our application by setting various HTTP headers_

### cors

_Enable cross-origin Requests_

### body-parser

_Parses the client’s request from json into javascript objects_

### jsonwebtoken

_Will handle the jwt operations for us_

### bcryptjs

_Help us to hash user passwords_

### typeorm

_The ORM we are going to use to manipulate database_

### reflect-metadata

_allow some annotations features used with TypeORM_

### class-validator

_A validation package that works really well with TypeORM_

### MySql
_We are going to use mysql2 as dev database_

### sqlite3 (if chosen)


the package we have to use is 'better-sqlite3'

### ts-node-dev

_Automatically restarts the server when we change any file_